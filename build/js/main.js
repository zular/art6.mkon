$(document).ready(function(){
    var maxHeight = 0;

    $("header .menu a").each(function(){
      if ( $(this).height() > maxHeight ) {
        maxHeight = $(this).height();
      }
    });

    $("header .menu a").height(maxHeight);
});

